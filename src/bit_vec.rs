pub(crate) struct BitVec {
    data: Box<[u8]>,
}

impl BitVec {
    /// Create a new bit vector with the given number of booleans.
    ///
    /// Each boolean is initialized to false.
    pub(crate) fn new(len: usize) -> Self {
        Self {
            data: vec![0; (len + 7) / 8].into_boxed_slice(),
        }
    }

    pub(crate) fn get(&self, i: usize) -> bool {
        let vec_i = i / 8;
        let byte_i = i % 8;
        let mask = 1 << byte_i;

        (self.data[vec_i] & mask) != 0
    }

    pub(crate) fn set(&mut self, i: usize) {
        let vec_i = i / 8;
        let byte_i = i % 8;
        let mask = 1 << byte_i;

        self.data[vec_i] |= mask;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::seq::SliceRandom;

    #[test]
    fn no_oob_panic() {
        for len in 0..100 {
            let vec = BitVec::new(len);
            for i in 0..len {
                assert!(!vec.get(i));
            }
        }
    }

    #[test]
    fn set_works() {
        const LEN: usize = 256;

        let mut ids = (0..LEN).collect::<Vec<usize>>();
        ids.shuffle(&mut rand::thread_rng());

        let mut vec = BitVec::new(LEN);
        for i in 0..LEN {
            vec.set(ids[i]);

            for j in 0..LEN {
                assert_eq!(j <= i, vec.get(ids[j]));
            }
        }
    }
}
