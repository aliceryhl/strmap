//! This crate provides a map using string (or path) keys.
//!
//! Keys are stored using the `fst` crate to compress similar strings.
//!
//! The crate is an experiment and not a serious project.

use rand::Rng;
use std::path::PathBuf;

mod bit_vec;
mod del_vec;
pub mod pathmap;
mod piece;
pub mod strmap;

pub use self::pathmap::PathMap;
pub use self::strmap::StrMap;

pub enum StrMapConfig {
    /// Store string map in memory.
    InMemory,
    /// Store string map on disk in the provided directory.
    OnDisk(PathBuf),
}

impl StrMapConfig {
    fn new_location(&self) -> Option<PathBuf> {
        use std::fmt::Write;

        match self {
            Self::InMemory => None,
            Self::OnDisk(dir) => {
                let mut path = dir.clone();

                let mut name = [0u8; 16];
                rand::thread_rng().fill(&mut name);
                let mut name_hex = String::with_capacity(32 + 4);
                for byte in name {
                    write!(name_hex, "{:x}", byte).unwrap();
                }
                name_hex.push_str(".dat");
                path.push(&name_hex);

                Some(path)
            }
        }
    }
}

struct DelOnDrop(PathBuf);
impl Drop for DelOnDrop {
    fn drop(&mut self) {
        let _ = std::fs::remove_file(&self.0);
    }
}
