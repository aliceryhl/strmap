use std::io::Write;
use strmap::{StrMap, StrMapConfig};

fn opt() -> StrMapConfig {
    let _ = std::fs::create_dir("./testdata");

    strmap::StrMapConfig::OnDisk("./testdata".into())
}

#[test]
fn insert_get() {
    const MAX: usize = 10000;

    let opts = opt();
    let mut map = StrMap::empty();

    for i in 0..MAX {
        let key = format!("{}", i);
        map.insert(key.as_bytes(), i);

        if map.should_rebalance() {
            map.rebalance(&opts).unwrap();
        }
    }

    let mut seen = vec![false; MAX];
    let mut curr = map.first();
    while let Some((key, val)) = curr {
        assert_eq!(key, format!("{}", val).as_bytes());
        seen[*val] = true;
        curr = map.next(&key);
    }

    seen.into_iter().for_each(|v| assert!(v));
}

#[test]
fn insert_delete_half() {
    const MAX: usize = 10000;

    let opts = opt();
    let mut map = StrMap::empty();

    for i in 0..MAX {
        let key = format!("{}", i);
        map.insert(key.as_bytes(), i);

        if map.should_rebalance() {
            map.rebalance(&opts).unwrap();
        }
    }

    for i in 0..MAX {
        if i % 2 == 1 {
            map.delete(format!("{}", i).as_bytes());

            if map.should_rebalance() {
                map.rebalance(&opts).unwrap();
            }
        }
    }

    let mut seen = vec![false; MAX];
    let mut curr = map.first();
    while let Some((key, val)) = curr {
        assert_eq!(key, format!("{}", val).as_bytes());
        seen[*val] = true;
        curr = map.next(&key);
    }

    for i in 0..MAX {
        assert_eq!(i % 2 == 0, seen[i], "{}", i);
    }
}

#[test]
fn build_delete_half() {
    const MAX: usize = 1000000;

    let opts = opt();

    let mut key_data = Vec::new();
    let mut key_slices = Vec::new();
    let mut vals = Vec::new();

    for i in 0..MAX {
        let a = key_data.len();
        write!(key_data, "{}", i).unwrap();
        key_slices.push((a, key_data.len()));
        vals.push(i);
    }
    let key_refs: Vec<&[u8]> = key_slices
        .into_iter()
        .map(|(a, b)| &key_data[a..b])
        .collect();

    let mut map = StrMap::build(&key_refs, vals, &opts).unwrap();

    for i in 0..MAX {
        if i % 2 == 1 {
            map.delete(format!("{}", i).as_bytes());

            if map.should_rebalance() {
                println!("{}", i);
                map.rebalance(&opts).unwrap();
            }
        }
    }

    let mut seen = vec![false; MAX];
    let mut curr = map.first();
    while let Some((key, val)) = curr {
        assert_eq!(key, format!("{}", val).as_bytes());
        seen[*val] = true;
        curr = map.next(&key);
    }

    for i in 0..MAX {
        assert_eq!(i % 2 == 0, seen[i], "{}", i);
    }
}

#[test]
fn build_insert() {
    const MAX1: usize = 100000;
    const MAX2: usize = 101000;

    let opts = opt();

    let mut key_data = Vec::new();
    let mut key_slices = Vec::new();
    let mut vals = Vec::new();

    for i in 0..MAX1 {
        let a = key_data.len();
        write!(key_data, "{}", i).unwrap();
        key_slices.push((a, key_data.len()));
        vals.push(i);
    }
    let key_refs: Vec<&[u8]> = key_slices
        .into_iter()
        .map(|(a, b)| &key_data[a..b])
        .collect();

    let mut map = StrMap::build(&key_refs, vals, &opts).unwrap();

    for i in MAX1..MAX2 {
        let key = format!("{}", i);
        map.insert(key.as_bytes(), i);

        if map.should_rebalance() {
            map.rebalance(&opts).unwrap();
        }
    }

    let mut seen = vec![false; MAX2];
    let mut curr = map.first();
    while let Some((key, val)) = curr {
        assert_eq!(key, format!("{}", val).as_bytes());
        seen[*val] = true;
        curr = map.next(&key);
    }

    for i in 0..MAX2 {
        assert!(seen[i], "{}", i);
    }
}
